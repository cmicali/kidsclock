/**
 * kidsclock
 */

#include <Arduino.h>

#include <Wire.h>
#include <RTClib.h>
#include <Adafruit_NeoPixel.h>
#include <Adafruit_LEDBackpack.h>
#include <Adafruit_GFX.h>
#include <Easing.h>
#include <OneButton.h>
#include <LinkedList.h>
#include <SimpleTimer.h>
#include <LEDFader.h>

#define NEOPIXEL_RING_PIN 6
#define BUTTON_LED_PIN 5
#define BUTTON_PIN 4

////////////////////////////////////////////////////////////////////
// Color Ring

struct Color {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

class RGBFader {

public:

    RGBFader() {
        color = {0, 0, 0};
        current_color = {0, 0, 0};
        target_color = {0, 0,0 };
    }

    virtual void loop();

    void fadeToColor(uint8_t r, uint8_t g, uint8_t b, uint32_t msec) {
        // If we are fading, start from current fade transition color
        if (isFading()) {
            color.r = current_color.r;
            color.g = current_color.g;
            color.b = current_color.b;
        }
        else {
            // Otherwise start from our set color
            current_color.r = color.r;
            current_color.g = color.g;
            current_color.b = color.b;
        }
        target_color.r = r;
        target_color.g = g;
        target_color.b = b;
        fade_start_time_ms = millis();
        fade_time_ms = msec;
    }

    virtual void setColor(uint8_t r, uint8_t g, uint8_t b);

    int isFading() {
        return (color.r != target_color.r) || (color.g != target_color.g) || (color.b != target_color.b);
    }

protected:

    long fade_start_time_ms, fade_time_ms;
    Color color, current_color, target_color;

    void handleLoop() {
        if (isFading()) {
            long pos = millis() - fade_start_time_ms;

            uint8_t new_r = clamp(color.r + pos * (target_color.r - color.r) / fade_time_ms);
            uint8_t new_g = clamp(color.g + pos * (target_color.g - color.g) / fade_time_ms);
            uint8_t new_b = clamp(color.b + pos * (target_color.b - color.b) / fade_time_ms);

            if (new_r != current_color.r ||
                new_g != current_color.g ||
                new_b != current_color.b) {
                current_color.r = new_r;
                current_color.g = new_g;
                current_color.b = new_b;
                setColor(new_r, new_g, new_b);
            }
            if (current_color.r == target_color.r &&
                current_color.g == target_color.g &&
                current_color.b == target_color.b) {
                color.r = target_color.r;
                color.g = target_color.g;
                color.b = target_color.b;
            }
        }
    }

    long clamp(long value, long min, long max) {
        if (value < min) value = min;
        if (value > max) value = max;
        return value;
    }

    uint8_t clamp(long value) {
        return (uint8_t)clamp(value, 0, 255);
    }

};

void RGBFader::loop() {
    handleLoop();
}
void RGBFader::setColor(uint8_t r, uint8_t g, uint8_t b) {}

class ColorRing : public RGBFader {
public:

    ColorRing(int pin) : RGBFader() {
        strip = Adafruit_NeoPixel(24, pin, NEO_GRB + NEO_KHZ800);
    }

    void setup() {
        strip.begin();
        strip.show();
    }

    void setBrightness(uint8_t brightness) {
        strip.setBrightness(brightness);
    }

    void setColor(uint8_t r, uint8_t g, uint8_t b) {
        uint32_t c = strip.Color(r, g, b);
        for(uint16_t i=0; i < strip.numPixels(); i++) {
            strip.setPixelColor(i, c);
        }
        strip.show();
    }

protected:
    Adafruit_NeoPixel strip;

};

////////////////////////////////////////////////////////////////////
// Clock

#define AM 0
#define PM 1

class Alarm {
public:
    Alarm() {}
    Alarm(int hour, int minute, void (*callback)()) {
        _hour = hour;
        _minute = minute;
        _callback = callback;
    }
    void handle(int currentHour, int currentMinute) {
        if (!fired) {
            if (currentHour == _hour && currentMinute == _minute) {
                fired = 1;
                Serial.println('Alarm fired');
                _callback();
            }
        }
        else {
            if (currentHour != _hour || currentMinute != _minute) {
                fired = 0;
            }
        }
    }
private:
    int fired = 0;
    int _hour, _minute;
    void (*_callback)();
};

class Clock {
public:

    int hour = 0;
    int minute = 0;
    int second = 0;

    Clock(uint8_t sevenSegmentDisplayAddress = 0x70) {
        displayAddress = sevenSegmentDisplayAddress;
    }

    void setup() {
        // Setup the display.
        display.begin(displayAddress);
        // Setup the DS1307 real-time clock.
        rtc.begin();

        bool setClockTime = !rtc.isrunning();
        // setClockTime = true;
        if (setClockTime) {
            Serial.println("Setting DS1307 time");
            rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
        }

        readRTC();
    }
    
    void setBrightness(uint8_t brightness) {
        display.setBrightness(brightness);
    }

    void addAlarm(int hour, int minute, int ampm, void (*callback)()) {
        if (ampm == PM) {
            hour += 12;
        }
        Alarm a = Alarm(hour, minute, callback);
        alarms.add(a);
    }

    void loop() {

        if (millis() - clock_counter < 1000) {
            return;
        }

        clock_counter = millis();

        if (second == 0) {
            readRTC();
        }

        int displayValue = hour*100 + minute;
        if (hour > 12) {
            displayValue -= 1200;
        }
        else if (hour == 0) {
            displayValue += 1200;
        }

        display.print(displayValue, DEC);
        blinkColon = !blinkColon;
        display.drawColon(blinkColon);
        display.writeDisplay();

        handleAlarms();

        second += 1;
        if (second > 59) {
            second = 0;
            minute += 1;
            if (minute > 59) {
                minute = 0;
                hour += 1;
                if (hour > 23) {
                    hour = 0;
                }
            }
        }

    }

private:

    Adafruit_7segment display = Adafruit_7segment();
    RTC_DS1307 rtc = RTC_DS1307();

    LinkedList<Alarm> alarms = LinkedList<Alarm>();

    uint8_t displayAddress;

    bool blinkColon = false;
    unsigned long clock_counter = 0;

    void readRTC() {
        DateTime now = rtc.now();
        hour = now.hour();
        minute = now.minute();
        second  = now.second();
    }

    void handleAlarms() {
        for (int i = 0; i < alarms.size(); i++) {
            Alarm a = alarms.get(i);
            a.handle(hour - 1, minute);
        }
    }

};


////////////////////////////////////////////////////////////////////
// Main

SimpleTimer timer = SimpleTimer();
Clock clock = Clock();
ColorRing colorRing = ColorRing(NEOPIXEL_RING_PIN);
OneButton button = OneButton(BUTTON_PIN, false);
LEDFader buttonLED = LEDFader(BUTTON_LED_PIN);

int c = 0;
void onButtonPressed() {
    buttonLED.set_value(0xFF);
    buttonLED.fade(0, 500);
    switch(c%8) {
        case 0:
            colorRing.fadeToColor(0xFF, 0x00, 0x00, 2000);
            break;
        case 1:
            colorRing.fadeToColor(0x00, 0xFF, 0x00, 2000);
            break;
        case 2:
            colorRing.fadeToColor(0xFF, 0x00, 0xFF, 2000);
            break;
        case 4:
            colorRing.fadeToColor(0x00, 0xFF, 0xFF, 2000);
            break;
        case 5:
            colorRing.fadeToColor(0xFF, 0xFF, 0x00, 2000);
            break;
        case 6:
            colorRing.fadeToColor(0xFF, 0x60, 0xFF, 2000);
            break;
        case 7:
            colorRing.fadeToColor(0xFF, 0xFF, 0xFF, 2000);
            break;
    }
    c++;
}

void bedtime() {
    Serial.println("bedtime");
    colorRing.fadeToColor(0xFF, 0, 0, 2000);
}

void fadeout() {
    Serial.println("fadeout");
    colorRing.fadeToColor(0x00, 0, 0, 3000);
}

void fadein() {
    Serial.println("fadein");
    colorRing.fadeToColor(0xFF, 0, 0, 3000);
}

void wakeup() {
    Serial.println("wakeup");
    colorRing.fadeToColor(0xFF, 0x40, 0xFF, 3000);
}

void longPressStart() {
    buttonLED.set_value(0xff);
}

void longPressFinished() {
    buttonLED.set_value(0xFF);
    buttonLED.fade(0, 500);
    colorRing.fadeToColor(0x00, 0, 0, 2000);
}

void setup() {

    // Setup Serial port to print debug output.
    Serial.begin(115200);
    Serial.println("kidsclock boot");

    pinMode(BUTTON_LED_PIN, OUTPUT);
    digitalWrite(BUTTON_LED_PIN, LOW);

    button.setClickTicks(50);
    button.setPressTicks(1000);
    button.attachClick(onButtonPressed);
    button.attachLongPressStart(longPressStart);
    button.attachLongPressStop(longPressFinished);

    colorRing.setup();
    colorRing.setBrightness(0x40); // 0-0xFF

    clock.setup();
    clock.setBrightness(4); // 0-0xF

    clock.addAlarm(7, 30, PM, bedtime);
    //clock.addAlarm(11, 00, PM, fadeout);
    clock.addAlarm(6, 30, AM, wakeup);
    clock.addAlarm(7, 30, AM, fadeout);

}

////////////////////////////////////////////////////////////////////
// Main loop

void loop() {
    button.tick();
    colorRing.loop();
    clock.loop();
    timer.run();
    buttonLED.update();
}

